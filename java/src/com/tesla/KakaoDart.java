package com.tesla;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


//http://tech.kakao.com/2017/09/27/kakao-blind-recruitment-round-1/
public class KakaoDart {

    static void solve2(String dart)
    {
        Pattern pattern = Pattern.compile("'(.*?)'");
        Matcher matcher = pattern.matcher(dart);

    }
    static void solve(String dart)
    {

    }

    public static void main(String[] args) {
        // write your code here
//        System.out.print (2223);

//        String input = "1S2D*3T10";
        String[] inputArr= new String[]{"1S2D*3T","1D2S#10S","1D2S0T","1S*2T*3S","1D#2S*3S","1T2D3D#","1D2S3T*"};


//        String regexe = "These";
//        Pattern pattern = Pattern.compile(regexe);
        String regNum ="\\d+";
        String regBonus ="[SDT]";
        String regOption ="[#*]";
        String regexe = regNum+regBonus+regOption+"?"; // ? is option
        Pattern pattern = Pattern.compile(regexe , Pattern.CASE_INSENSITIVE);
        Pattern patternNum = Pattern.compile(regNum );
        Pattern patternBonus = Pattern.compile(regBonus );
        Pattern patternOption = Pattern.compile(regOption );

        int score[] = new int[3];
        int bonus[] = new int[3];
        int option[] = new int[3];
        for (String input : inputArr) {
            System.out.println("\ninput="+input);
            Matcher matcher = pattern.matcher(input);

            int trial=0;
            int sum=0;
            while (matcher.find()) {
                System.out.println("find() found : " + matcher.group() + " start at " + matcher.start() + " end at " + matcher.end());
                String oneResult = matcher.group();
                Matcher m = patternNum.matcher(oneResult);
                m.find();
                String numStr = m.group();
//                System.out.println("numStr="+numStr);
                score[trial] = Integer.parseInt(numStr);

                char bonusChar = oneResult.charAt(m.end());
//                m= patternBonus.matcher(oneResult);
//                String bonusChar = m.group();
                if (bonusChar=='S') {
                    bonus[trial] =1;
                } else
                if (bonusChar=='D') {
                    bonus[trial] = 2;
                } else
                if (bonusChar=='T') {
                    bonus[trial] = 3;
                }

                score[trial] = (int) Math.pow(score[trial], bonus[trial]);

                m=patternOption.matcher(oneResult);  //"[#*]?"; ?포함하면 공백까지 찾음.
                String optionStr=null;
                if (m.find()) {
                    optionStr = m.group();
                    if (optionStr.equals("*")){
                        score[trial] *=2;
                        if (trial>0) score[trial-1] *=2;
                    } else
                    if (optionStr.equals("#")){
                        score[trial] *=-1;
                    }
                }
//                char optionChar = oneResult.charAt(m.end());

//                System.out.printf("\n score[%d]=%d for numStr %s \n",trial,score,numStr);
                System.out.printf(" score[%d]=%d bonus=%d option= %s \n",trial,score[trial],bonus[trial], optionStr);

                trial ++;
            }

            sum= score[0]+score[1]+score[2];
            System.out.println("sum="+sum);
        }

//        solve(5, new int[]{9,20,28,18,11}, new int[]{30, 1, 21, 17, 28} );
    }
}
