package com.tesla;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatcherExample {

    static void solve2(String dart)
    {
        Pattern pattern = Pattern.compile("'(.*?)'");
        Matcher matcher = pattern.matcher(dart);

    }
    static void solve(String dart)
    {
        int index=0;
        int savedNum=0;
        int currentNum=0;
        int sum =0;
        while (index < dart.length())
        {
            char ch =dart.charAt(index);
            if (ch >='0' && ch <='9') {
                currentNum = ch-'0' + savedNum*10;
                savedNum=0;
                continue;
            }

            switch (ch){
                case 'S':
                    sum = currentNum;
                    break;
                case 'D':
                    sum = currentNum * currentNum;
                    break;
                case 'T':
                    sum = currentNum * currentNum*currentNum;
                    break;
                case '*':

                    break;
                case '#':
                    break;
            }


        }
    }

    public static void main(String[] args) {
        // write your code here
//        System.out.print (2223);

//        String input = "This is an apple. These are 33 (thirty-three) apples";
        String input = "2D*";
        String regexe = "[#*]";
//        String regexe = "These";
//        Pattern pattern = Pattern.compile(regexe);
        Pattern pattern = Pattern.compile(regexe , Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(input);

        while (matcher.find()) {
            System.out.println("find() found : "+ matcher.group() +" start at "+ matcher.start() +" end at "+matcher.end());
        }

        // Use method matches()
        if (matcher.matches()) {
            System.out.println(" matches() found the pattern \"" + matcher.group()
                    + "\" starting at index " + matcher.start()
                    + " and ending at index " + matcher.end());
        } else {
            System.out.println("matches() found nothing");
        }

        // Use method lookingAt()
        if (matcher.lookingAt()) {
            System.out.println("lookingAt() found the pattern \"" + matcher.group()
                    + "\" starting at index " + matcher.start()
                    + " and ending at index " + matcher.end());
        } else {
            System.out.println("lookingAt() found nothing");
        }

//        solve(5, new int[]{9,20,28,18,11}, new int[]{30, 1, 21, 17, 28} );
    }
}
