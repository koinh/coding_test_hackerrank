package com.tesla;

import java.util.Scanner;

public class KakaoMap {


    static void solve(int n, int[] arr1, int[] arr2) {

        for (int i=0; i<n; i++)
        {
            int decoded = arr1[i] | arr2[i];
            for (int j=n-1; j>=0; j--) {
                int shift = 1<<j;
                if (( decoded & shift ) >0) {
                    System.out.print("#");
                } else
                {
                    System.out.print(" ");
                }
            }
            System.out.print("\n");
        }

    }


    public static void main(String[] args) {
	// write your code here
//        System.out.print (2223);

//        Scanner in = new Scanner(System.in);
//        int m = in.nextInt();

        solve(5, new int[]{9,20,28,18,11}, new int[]{30, 1, 21, 17, 28} );
    }

}
