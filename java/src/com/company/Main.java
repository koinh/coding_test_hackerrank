package com.company;

// 계산기 만들기
// Calculator 클래스의 evaluate 함수를 완성시켜, 모든 테스트케이스를 통과시켜보시오.

import java.util.StringTokenizer;

class Calculator {
    public int evaluate(String expression) {
        int sum = 0;

        //String strArr[] = expression.split("[+]");
        int len = expression.length();
        int index = 0;
        while (index < len){
            index = expression.indexOf("+",index);
            if (index >=0 ) {
                String sub = expression.substring(0,index);
                int num = Integer.parseInt(sub);
                sum +=num;
                System.out.println(num);

                expression = expression.substring(index+1);
                index =0;
            } else
            {
                int num = Integer.parseInt(expression);
                sum+=num;
                System.out.println(num);
                break;
            }
        }

        return sum;
    }
}

public class Main {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Calculator calculator = new Calculator();
        int sum = calculator.evaluate("1+2+3");

        System.out.println("1+2+3 = " + sum);

//        String str="this+is-a*test/10";
//        StringTokenizer st = new StringTokenizer(str);
//        while (st.hasMoreTokens()) {
//            System.out.println(st.nextToken());
//        }
//
//        String[] result = str.split("[+]|-|[*]|/");
//        for (String s : result)
//        {
//            System.out.println(s);
//        }
    }
}